import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // declare resource in try
        try (MyResource resource = new MyResource()) {
            resource.doSomething();
        } catch (Exception e) {
            System.out.println("Regular exception: " + e.toString());

            // getting the array of suppressed exceptions, and its length
            Throwable[] suppressedExceptions = e.getSuppressed();
            int n = suppressedExceptions.length;

            if (n > 0) {
                System.out.println("We've found " + n + " suppressed exceptions:");
                for (Throwable exception : suppressedExceptions) {
                    System.out.println(exception.toString());
                }
            }
        }

        // exception
        RegistrationService service = new RegistrationService();
        try {
            service.validateEmailException("abc@gmail.com");
        } catch (EmailNotUniqueException e) {
            // logging and handling the situation
            System.out.println(e.toString());
        }

        // runtimeException
        service.validateEmailRuntimeException("abc@gmail1.com");


    }
}
